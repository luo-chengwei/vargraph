#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Setup script for varGraph

You can install varGraph with

python setup.py install
"""
from glob import glob
import os
import sys
if os.path.exists('MANIFEST'):
    os.remove('MANIFEST')

from setuptools import setup

if sys.argv[-1] == 'setup.py':
    print("To install, run 'python setup.py install'")
    print()

if sys.version_info[:2] < (2, 7):
    print("varGraph requires Python 2.7 or later (%d.%d detected)." %
          sys.version_info[:2])
    sys.exit(-1)

# Write the version information.
sys.path.insert(0, 'varGraph')
import release
date, date_info, version, version_info = release.get_info()
sys.path.pop(0)

packages=["varGraph",
          "varGraph.algorithms",
          "varGraph.utils",
          "varGraph.classes",
          "varGraph.exception"]

docdirbase  = 'share/doc/varGraph-%s' % version
# add basic documentation
data = [(docdirbase, glob("*.txt"))]

install_requires = ['decorator>=3.4.0',
					'networkx>=1.10',
					'Biopython>=1.66']


if __name__ == "__main__":

    setup(
        name             = release.name.lower(),
        version          = version,
        author           = release.authors['Chengwei'][0],
        author_email     = release.authors['Chengwei'][1],
        description      = release.description,
        keywords         = release.keywords,
        long_description = release.long_description,
        license          = release.license,
        platforms        = release.platforms,
        url              = release.url,
        install_requires = install_requires,
        zip_safe         = False,
        packages		 = packages,
    )
