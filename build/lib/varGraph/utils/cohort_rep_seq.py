#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

"""The utility script for generate the representative sequences for a cohort configured in
a configuration file.

This script is part of the varGraph package under MIT license.
"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

USAGE = \
"""Usage: 
%prog [options] -p/--proj <varGraph project directory> -c/--config <sample config file>

The sample config file should follow the format:

sample_1	group_A
sample_2	group_B
sample_3	group_A
sample_4	group_C
sample_5	group_B
sample_6	group_B
......
sample_N	group_C

The sample_x IDs should match the sample IDs used in project configuration.

Add --help to see a full list of required and optional
arguments to run the script.

Additional information can also be found at:
https://bitbucket.org/luo-chengwei/vargraph/wiki


varGraph: expanding variation analysis in series metagenomes

If you use varGraph in your work, please cite it as:
<varGraph citation here>

Copyright: Chengwei Luo, Georgia Institute of Technology, 2016.
"""

import sys, os, re, glob, cPickle, glob
from operator import itemgetter
from optparse import OptionParser, OptionGroup
#from subprocess import call, Popen, PIPE
#import multiprocessing as mp

from varGraph.algorithms import nucmer_init
from varGraph import *

################################### FUNCTIONS ######################################


def gen_rep_seq(proj_dir, outfile, sample_group=None, samples=None, 
			len_thr = 500, identity_thr = 0.97, verbose = False):
	"""generate the representative sequences for a cohort.
	
	Parameters
	----------
	proj_dir: varGraph project directory. It can be initiated by algorithms.vargraph_project.project_construct()
	outfile: the output file that will hold the rep_seqs in FastA format 
	
	Optional parameters
	-------------------
	sample_group: the name of sample group, if None, then use 'All'
	samples: the list of samples used to generate the rep seqs
	len_thr: the minimum length to be used in this rep seq generation
	identity_thr: the minimum alignment nucleotide identity to be used in this re seq generation
	verbose: Default to be false. If turned on, prints out runtime information to help debug.
	
	"""
	if sample_group == None: sample_group = 'All'
	node_dict_pkl = '%s/node_dict.pkl' % proj_dir
	metaseq_dir = '%s/MetaSeqs' % proj_dir
	if not os.path.exists(node_dict_pkl):
		sys.stderr.write('[FATAL] Cannot locate node_dict.pkl in the varGraph directory you supplied.\n')
		exit(1)
	if not os.path.exists(metaseq_dir):
		sys.stderr.write('[FATAL] Cannot locate the MetaSeq directory that is supposed to hold the batch PKLs of MetaSeq objs.\n')
		exit(1)
	
	metaseq_pkls = glob.glob('%s/*.pkl' % metaseq_dir)
	if metaseq_pkls == None:
		sys.stderr.write('[FATAL] The MetaSeq directory appears to have zero MetaSeq pkl.\n')
		exit(1)
	
	out_fa = '%s/rep_seq.fa' % (metaseq_dir)
	fa_fh = open(outfile, 'wb')
	# load node_dict
	if verbose: sys.stdout.write('[gen_rep_seq] Loading node dict.\n')
	node_dict = cPickle.load(open(node_dict_pkl, 'rb'))
	if verbose: sys.stdout.write('[gen_rep_seq] Loaded node dict.\n')
	for metaseq_pkl in metaseq_pkls:
		ind = int(re.search('batch\_(\d+).pkl', os.path.basename(metaseq_pkl)).group(1))
		meta_seqs = cPickle.load(open(metaseq_pkl, 'rb'))
		if verbose: sys.stdout.write('[gen_rep_seq] Batch #%i loaded.\n' % (ind))
		rep_seqs = []
		for meta_seq_tag in meta_seqs:
			seqs = map(itemgetter(0), meta_seqs[meta_seq_tag].get_rep_seqs(samples=samples,\
						 len_thr = len_thr, identity_thr=identity_thr))
			rep_seqs.append([meta_seq_tag, seqs])
		if verbose: sys.stdout.write('[gen_rep_seq] Batch #%i finished.\n' % (ind))
	
		for mt_tag, seqs in rep_seqs:
			for ind, seq in enumerate(seqs, 1):
				tag = 'samples_%s|len_thr=%i|identity_thr=%2.2f|rep_seq_%s.%i' % (sample_group, len_thr, identity_thr, mt_tag, ind)
				fa_fh.write('>%s\n%s\n' % (tag, seq))
	fa_fh.close()
	if verbose: sys.stdout.write('[gen_rep_seq] Output file written: %s\n' % (outfile))
	return 0

	

################################### MAIN ######################################
def main(argv = sys.argv[1:]):

	parser = OptionParser(usage = USAGE, version="Version: " + varGraph.__version__)

	# Compulsory arguments
	compOptions = OptionGroup(parser, "Compulsory parameters",
						"There options are compulsory, and may be supplied in any order.")
	
	compOptions.add_option("-p", "--proj", type = "string", metavar = "DIR",
							help = "The varGraph project directory.")
	
	compOptions.add_option("-c", "--config", type = "string", metavar = "FILE",
							help = "The sample configuration file.")
	
	parser.add_option_group(compOptions)

	# Optional arguments that need to be supplied if not the same as default
	optOptions = OptionGroup(parser, "Optional parameters",
						"There options are optional, and may be supplied in any order.")

	optOptions.add_option("-i", "--identity", type = "float", default = 0.97, metavar = "FLOAT",
							help = "The minimum identity (0.9-1.0) required to construct rep seq. [default: 0.97]")
	
	optOptions.add_option("-l", "--len_thr", type = "int", default = 500, metavar = "INT",
							help = "The minimum length requirement for rep seq to be recorded. [default: 500]")
	
	optOptions.add_option("--verbose", default = False, action = "store_true",
							help = "Print out runtime information, helpful in debug, \
							otherwise, only important warnings/errors will show [default: False].")
								
	parser.add_option_group(optOptions)
	
							
	# parse input files and/or metaphlan files from commandline
	try:
		options, x = parser.parse_args(argv)
	except:
		parser.error('[FATAL]: parsing failure.\n')
		exit(1)
	
	if not options.proj:
		parser.error('[FATAL]: output directory is not supplied, you must specify output prefix by -p/--proj.\n')
		exit(1)
	
	if not options.config or not os.path.exists(options.config):
		parser.error('[FATAL]: sample configuration file is not supplied, you must specify the configuration file by -c/--config.\n')
		exit(1)
	
	if options.identity < 0.7 or options.identity >1:
		sys.stderr.write('[WARNING]: Cannot have identity outside of [0.7, 1.0], will use %.2f instead.\n' % options.identity)
		options.identity = 0.97
	
	proj_info_pkl = '%s/proj_info.pkl' % options.proj
	node_dict_pkl = '%s/node_dict.pkl' % options.proj
	if not os.path.exists(proj_info_pkl):
		sys.stderr.write('[FATAL] cannot locate proj_info.pkl in the varGraph project directory you supplied.\n')
		exit(1)
	if not os.path.exists(node_dict_pkl):
		sys.stderr.write('[FATAL] cannot locate node_dict.pkl in the varGraph project directory you supplied.\n')
		exit(1)
	
	# load basic info
	samples, assemblies, coords_files = cPickle.load(open(proj_info_pkl, 'rb'))
	# parse sample config
	sample_groups = {}
	for line in open(options.config, 'rb'):
		s, g = line.rstrip().split('\t')
		if s not in samples:
			sys.stderr.write('[FATAL] Sample %s supplied in sample config file does not exists in this varGraph project.\n' % s)
			exit()
		if g not in sample_groups: sample_groups[g] = []
		sample_groups[g].append(s)

	
	rep_seq_dir = '%s/rep_seqs' % options.proj
	if not os.path.exists(rep_seq_dir): os.mkdir(rep_seq_dir)
	
	# start gen_rep_seq
	for g in sample_groups:
		outfile = '%s/%s.rep_seq.fa' % (rep_seq_dir, g)
		group_samples = sample_groups[g]
		gen_rep_seq(options.proj, outfile, sample_group=g, samples = group_samples, len_thr = options.len_thr, 
					identity_thr = options.identity, verbose = options.verbose)
	
	
			
if __name__ == '__main__': main()

