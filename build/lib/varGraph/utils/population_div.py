#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

"""The utility script for varGraph project construction for a cohort configured in a configuration file.

This script is part of the varGraph package under MIT license.
"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.


import sys, os, re, glob, cPickle
import random
import sqlite3
import argparse
from itertools import combinations

from varGraph.algorithms import nucmer_init
from varGraph import *
from varGraph.algorithms.sql import fasta2sql, batch_query_seq

from varGraph.classes.metaseq import MetaSeq, subgraph2metaSeq
from varGraph.classes.seqnode import SeqNode
from varGraph.classes.aln import AlnDetails


class projParser(object):
	def __init__(self):
		parser = argparse.ArgumentParser(
					description='Parse arguments for population_div.py',
					usage = 'population_div.py <command> [<command options>]\n'+\
							'The commands are:\n'+\
							'\tget_repseq\tGenerates the representative sequences for metaseq objs.\n',
					version = "Version: " + varGraph.__version__)
		parser.add_argument('command', help='Subcommand to run')
		# parse_args defaults to [1:] for args, but you need to
		# exclude the rest of the args too, or validation will fail
		args = parser.parse_args(sys.argv[1:2])
		if not hasattr(self, args.command):
			sys.stderr.write('Unrecognized command.\n')
			parser.print_help()
			exit(1)
		# use dispatch pattern to invoke method with same name
		getattr(self, args.command)()
	
	def get_repseq(self):
		parser = argparse.ArgumentParser(description = \
					'Generate the representative sequences for a metaseq obj.')
		
		parser.add_argument("-p", "--proj_dir", type = str, metavar = "DIR",
							help = "The varGraph directory that holds the project.")
		
		parser.add_argument("--min_len", default = 500, metavar = "INT", 
							help = "Minimum length for metaseq construction [default: 500].")
	
		parser.add_argument("--min_identity", default = 0.97, metavar = "FLOAT", 
							help = "Minimum percentage Nt identity for metaseq construction [default: 97].")
											
		args = parser.parse_args(sys.argv[2:])
		
		if args.proj_dir == None or not os.path.exists(args.proj_dir):
			sys.stderr.write('[FATAL] Cannot locate varGraph project directory: %s\n' % args.proj_dir)
			exit(1)
		if args.min_len < 300:
			sys.stderr.write('minimum length cannot be less than 300bp, reset to 500bp now.\n' % proj.min_len)
			args.min_len=500
		if args.min_identity > 1 or args.min_identity < 0.9:
			sys.stderr.write('minimum nt identity should be in range of [0.90, 1.00].\n')
			exit(1)
			
		sys.stdout.write('Running population_div get_repseq.\n  proj_dir=%s\n  nt_identity_thr=%.3f\n  alignment_len_thr=%i\n\n' 
						% (args.proj_dir, args.min_identity, args.min_len))
		return args
		
################################### MAIN ######################################
def main():
	options = projParser()
	if hasattr(options, 'get_repseq'):
		x_args = options.get_repseq()
		min_len, nt_iden = x_args.min_len, x_args.min_identity
		proj_dir = x.args.proj_dir
		# parse every batch of metaseq objects into rep seqs
		
	
	
if __name__ == '__main__': main()
