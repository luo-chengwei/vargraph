#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

"""The utility script for generate the representative sequences for a cohort configured in
a configuration file.

This script is part of the varGraph package under MIT license.
"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

USAGE = \
"""Usage: 
%prog [options] -c/--conf <config.file> -o/--outprefix <output prefix>

The config file should follow the format:

[fa]
sample_1	/dir/to/sample_1/assembly/fasta/file   # tab-delimited
..... # You can add as many samples as you wish
sample_N	/dir/to/sample_N/assembly/fasta/file
[coords]
sample_1	sample_2	/dir/to/sample_1_vs_sample_2_coords_file 
sample_1	sample_3	/dir/to/sample_1_vs_sample_3_coords_file 
...
sample_N-1	sample_N	/dir/to/sample_N-1_vs_sample_N_coords_file 


Add --help to see a full list of required and optional
arguments to run the script.

Additional information can also be found at:
https://bitbucket.org/luo-chengwei/vargraph/wiki


varGraph: expanding variation analysis in series metagenomes

If you use varGraph in your work, please cite it as:
<varGraph citation here>

Copyright: Chengwei Luo, Georgia Institute of Technology, 2016.
"""

import sys, os, re, glob, cPickle, glob
from optparse import OptionParser, OptionGroup
#from itertools import combinations

from varGraph.algorithms import nucmer_init
from varGraph.utils.vargraph_project import project_construct, parse_config
from varGraph import *

################################### FUNCTIONS ######################################


def gen_rep_seq(proj_dir, node_dict_pkl=None, metaseq_pkls = None,\
				 samples=None, len_thr = 500, nt_thr = 0.97, verbose = False, cleanup=False):
	"""generate the representative sequences for a cohort.
	
	Parameters
	----------
	proj_dir: varGraph project directory. It can be initiated by algorithms.vargraph_project.project_construct()
	
	Optional parameters
	-------------------
	node_dict_pkl: path to the node_dict pickle file, it is returned by project_construct(),
					default is None and the function will try to locate it in the project directory.
	metaseq_pkls: the list of paths to the metaseq_pkls. it is returned by project_construct(),
					default is None and the function will try to locate the files in the project directory.
	
	
	"""

	if node_dict_pkl == None:
		node_dict_pkl = '%s/node_dict.pkl' % proj_dir
	try: node_dict = cPickle.load(open(node_dict_pkl, 'rb'))
	except: 
		sys.stderr.write('[FATAL]: Please examine input project directory, node_dict.pkl impaired.\n')
		exit(1)
	
	if metaseq_pkls == None:
		metaseq_pkls = glob.glob('%s/MetaSeqs/*.pkl' % proj_dir)
	if len(metaseq_pkls) < 1:
		sys.stderr.write('[FATAL]: Please examine input project directory, no MetaSeq pkl found.\n')
		exit(1)
	
	out_fa = '%s/rep_seq.fa' % (proj_dir)
	fa_fh = open(out_fa, 'wb')
	for metaseq_pkl in metaseq_pkls:
		ind = int(re.search('batch\_(\d+).pkl', os.path.basename(metaseq_pkl)).group(1))
		meta_seqs = cPickle.load(open(metaseq_pkl, 'rb'))
		
		path_dict = {}
		graph_dict = {}
		gfa_dict = {}
		# define files
		path_pkl = '%s/MetaSeqs/rep_path_%i.pkl' % (proj_dir, ind)
		graph_pkl = '%s/MetaSeqs/rep_vargraph_%i.pkl' % (proj_dir, ind)
		gfa_pkl = '%s/MetaSeqs/gfa_strings_%i.pkl' % (proj_dir, ind)
		
		for metaseq_tag in meta_seqs:
			meta_seq = meta_seqs[metaseq_tag]
			subind = 0
			for seq, path, X, gfa_strings in meta_seq.get_rep_seqs(samples = samples, \
										len_thr = len_thr, nt_thr = nt_thr):
				subind+=1
				tag = '%s.%i.%ibp' % (metaseq_tag, subind, len(seq))
				fa_fh.write('>%s\n%s\n' % (tag, seq))
	
			path_dict[tag] = path
			graph_dict[tag] = X
			gfa_dict[tag] = gfa_strings
		cPickle.dump(path_dict, open(path_pkl, 'wb'))
		cPickle.dump(graph_dict, open(graph_pkl, 'wb'))
		cPickle.dump(gfa_dict, open(gfa_pkl, 'wb'))
		if verbose:
			sys.stdout.write('[gen_rep_seq] %i batch finished.\n' % ind)
	fa_fh.close()
	
	# cleanup, remove 

	

################################### MAIN ######################################
def main(argv = sys.argv[1:]):

	parser = OptionParser(usage = USAGE, version="Version: " + varGraph.__version__)

	# Compulsory arguments
	compOptions = OptionGroup(parser, "Compulsory parameters",
						"There options are compulsory, and may be supplied in any order.")
	
	compOptions.add_option("-o", "--outdir", type = "string", metavar = "DIR",
							help = "The output directory.")
	
	compOptions.add_option("-c", "--config", type = "string", metavar = "FILE",
							help = "The configuration file of the varGraph rep_seq project.")
	
	parser.add_option_group(compOptions)

	# Optional arguments that need to be supplied if not the same as default
	optOptions = OptionGroup(parser, "Optional parameters",
						"There options are optional, and may be supplied in any order.")

	optOptions.add_option("-i", "--identity", type = "float", default = 0.97, metavar = "FLOAT",
							help = "The minimum identity (0.7-1.0) required to construct rep seq. [default: 0.97]")
	
	optOptions.add_option("-l", "--len_thr", type = "int", default = 500, metavar = "INT",
							help = "The minimum length requirement for rep seq to be recorded. [default: 500]")
	
	optOptions.add_option("--cleanup", default = False, action = "store_true",
							help = "Keeps only the essential files in the project directory. [default: False].")
	
	optOptions.add_option("--verbose", default = False, action = "store_true",
							help = "Print out runtime information, helpful in debug, \
							otherwise, only important warnings/errors will show [default: False].")
								
	parser.add_option_group(optOptions)
	
							
	# parse input files and/or metaphlan files from commandline
	try:
		options, x = parser.parse_args(argv)
	except:
		parser.error('[FATAL]: parsing failure.\n')
		exit(1)
	
	if not options.outdir:
		parser.error('[FATAL]: output directory is not supplied, you must specify output prefix by -o/--outdir.\n')
		exit(1)
	
	if not options.config or not os.path.exists(options.config):
		parser.error('[FATAL]: configuration file is not supplied, you must specify the configuration file by -c/--config.\n')
		exit(1)
	
	if options.identity < 0.7 or options.identity >1:
		sys.stderr.write('[WARNING]: Cannot have identity outside of [0.7, 1.0], will use %.2f instead.\n' % options.identity)
		options.identity = 0.97
	
	samples, assemblies, coords_files = parse_config(options.config)
	node_dict_pkl = '%s/node_dict.pkl' % options.outdir
	if not os.path.exists(node_dict_pkl):
		project_construct(samples, assemblies, coords_files, options.outdir, verbose = options.verbose)
	gen_rep_seq(options.outdir, len_thr = options.len_thr, nt_thr = options.identity, \
				verbose = options.verbose, cleanup=options.cleanup)
	
	
			
if __name__ == '__main__': main()

