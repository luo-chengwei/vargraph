#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

"""The utility script for varGraph project construction for a cohort configured in
a configuration file.

This script is part of the varGraph package under MIT license.
"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

USAGE = \
"""Usage: 
%prog [options] -c/--conf <config.file> -o/--outprefix <output prefix>

The config file should follow the format:

[fa]
sample_1	/dir/to/sample_1/assembly/fasta/file   # tab-delimited
..... # You can add as many samples as you wish
sample_N	/dir/to/sample_N/assembly/fasta/file
[coords]
sample_1	sample_2	/dir/to/sample_1_vs_sample_2_coords_file 
sample_1	sample_3	/dir/to/sample_1_vs_sample_3_coords_file 
...
sample_N-1	sample_N	/dir/to/sample_N-1_vs_sample_N_coords_file 


Add --help to see a full list of required and optional
arguments to run the script.

Additional information can also be found at:
https://bitbucket.org/luo-chengwei/vargraph/wiki


varGraph: expanding variation analysis in series metagenomes

If you use varGraph in your work, please cite it as:
<varGraph citation here>

Copyright: Chengwei Luo, Georgia Institute of Technology, 2016.
"""

import sys, os, re, glob, cPickle
import random
import sqlite3
from optparse import OptionParser, OptionGroup
from itertools import combinations

from varGraph.algorithms import nucmer_init
from varGraph import *
from varGraph.algorithms.sql import fasta2sql, batch_query_seq

from varGraph.classes.metaseq import MetaSeq, subgraph2metaSeq
from varGraph.classes.seqnode import SeqNode
from varGraph.classes.aln import AlnDetails

################################### FUNCTIONS ######################################
def chunks(l, n):
	"""Yield successive n-sized chunks from l."""
	for i in range(0, len(l), n):
		yield l[i:i+n]

def parse_config(config_file):
	assemblies = {}
	coords_files = {}
	flag = 0
	try:
		for line in open(config_file, 'rb'):
			if line[:-1] == '[fa]': flag = 1; continue
			if line[:-1] == '[coords]': flag = 2; continue
			if flag == 1:
				try: sample, fa = line[:-1].split('\t')
				except:raise Exception('Error in parsing assembly line.')
				if not os.path.exists(fa):
					raise Exception('Error in parsing assembly file.')
				assemblies[sample] = fa
			elif flag == 2:
				try: s1, s2, f = line[:-1].split('\t')
				except: raise Exception('Error in parsing coords file line.')
				if not os.path.exists(f):
					raise Exception('Error in parsing coords file.')
				coords_files[(s1, s2)] = f
			else:
				raise Exception('Error in parsing config file.')
	except Exception as e:
		sys.stderr.write('[FATAL]: %s.\n' % e)
		exit(1)
	samples = assemblies.keys()
	for s1, s2 in list(combinations(samples, 2)):
		if (s1, s2) not in coords_files and (s2, s1) not in coords_files:
			sys.stderr.write('[FATAL]: cannot locate coords file for %s vs %s.\n' % (s1, s2))
	return samples, assemblies, coords_files

def project_construct(samples, assemblies, coords_files, outdir, \
			identity_thr = 90, aln_len_thr = 500, verbose = False):
	sqlite_db = '%s/proj_db.sqlite' % outdir
	subgraph_pkl = '%s/subgraphs.pkl' % outdir
	
	if not os.path.exists(outdir):
		try: os.mkdir(outdir)
		except:
			sys.stderr.write('[FATAL] Cannot create the output directory you supplied: %s\n' % (outdir))
			exit(1)
			
	if not os.path.exists(subgraph_pkl):
		nucmer_init.init_from_nucmer(samples, assemblies, coords_files, outdir, 
							identity_thr = identity_thr, aln_len_thr = aln_len_thr, verbose = verbose)
	if verbose: sys.stdout.write('Subgraph objects initiated!\n')

def metaseq_batch_init(samples, outdir, identity_thr = 90, aln_len_thr = 500,verbose = False):
	metaseq_dir = '%s/MetaSeqs/' % outdir
	sqlite_db = '%s/proj_db.sqlite' % outdir
	subgraph_pkl = '%s/subgraphs.pkl' % outdir
	node_dict_pkl = '%s/node_dict.pkl' % outdir

	if not os.path.exists(metaseq_dir):
		try: os.mkdir(metaseq_dir)
		except:
			sys.stderr.write('[FATAL] Cannot create the metaseq directory you supplied.\n')
			exit(1)
			
	done_flag = '%s/done' % metaseq_dir
	if os.path.exists(done_flag): return 0
	
	# make meta_seq in batch mode
	meta_seqs = []
	# split subgraphs into 10K batches
	# load and shuffle subgraphs first
	subgraphs = cPickle.load(open(subgraph_pkl, 'rb'))
	random.shuffle(subgraphs)	
	if verbose:
		sys.stdout.write('[utils::vargraph_project::metaseq_batch_init] %i subgraphs for MetaSeq objects construction found.\n' % (len(subgraphs)))
	subgraph_batches = list(chunks(subgraphs, 10000))
	num_batches = len(subgraph_batches)
	if verbose:
		sys.stdout.write('[utils::vargraph_project::metaseq_batch_init] %i batches for MetaSeq construction.\n' % num_batches)
	
	# batch by batch mode
	metaseq_pkls = []
	node_dict = {}
	for batch_ind, subgraph_batch in enumerate(subgraph_batches, 1):
		key_list = []
		for subgraph in subgraph_batch:
			for node, node_info in subgraph.nodes(data=True):
				key_list.append(node_info['seq_ind'])
		batch_seqs = batch_query_seq(sqlite_db, key_list, verbose=verbose)
		meta_seqs = {}
		for subgraph_ind, subgraph in enumerate(subgraph_batch, 1):
			meta_seq_tag = '%i.%i' % (batch_ind, subgraph_ind)
			for node, node_info in subgraph.nodes(data=True):
				seq_ind = node_info['seq_ind']
				seq_sample, seq_tag, sequence = batch_seqs[seq_ind]
				node_dict[(seq_sample, seq_tag)] = (meta_seq_tag, seq_ind)
				seqnode = SeqNode(seq_sample, seq_tag, sequence)
				subgraph.node[node]['seqnode'] = seqnode
			meta_seq = subgraph2metaSeq(subgraph)
			meta_seq.update_seq(samples=samples)
			meta_seqs[meta_seq_tag] = meta_seq
		# preserved the results
		metaseq_pkl = '%s/metaseq_batch_%i.pkl' % (metaseq_dir, batch_ind)
		cPickle.dump(meta_seqs, open(metaseq_pkl, 'wb'))
		metaseq_pkls.append(metaseq_pkl)
		if verbose:
			sys.stdout.write('[utils::vargraph_project::metaseq_batch_init] [%i/%i] batches finished.\n' % (batch_ind, num_batches))
		else:
			sys.stdout.write('[%i/%i] batches finished.\n' % (batch_ind, num_batches))
		
	node_dict_pkl = '%s/node_dict.pkl' % (outdir)
	cPickle.dump(node_dict, open(node_dict_pkl, 'wb'))
	with open('%s/done' % metaseq_dir, 'wb') as dfh: dfh.write('1\n')
	
	return 0

	
################################### MAIN ######################################
def main(argv = sys.argv[1:]):

	parser = OptionParser(usage = USAGE, version="Version: " + varGraph.__version__)

	# Compulsory arguments
	compOptions = OptionGroup(parser, "Compulsory parameters",
						"There options are compulsory, and may be supplied in any order.")
	
	compOptions.add_option("-o", "--outdir", type = "string", metavar = "DIR",
							help = "The output directory.")
	
	compOptions.add_option("-c", "--config", type = "string", metavar = "FILE",
							help = "The configuration file of the varGraph rep_seq project.")
	
	parser.add_option_group(compOptions)
	
	
	# Optional arguments that need to be supplied if not the same as default
	optOptions = OptionGroup(parser, "Optional parameters",
						"There options are optional, and may be supplied in any order.")
	
	optOptions.add_option("--min_len", default = 500, metavar = "INT", 
							help = "Minimum length for metaseq construction [default: 500].")
	
	optOptions.add_option("--min_identity", default = 90, metavar = "FLOAT", 
							help = "Minimum percentage Nt identity for metaseq construction [default: 90].")
							
	optOptions.add_option("--verbose", default = False, action = "store_true",
							help = "Print out runtime information, helpful in debug, \
							otherwise, only important warnings/errors will show [default: False].")
								
	parser.add_option_group(optOptions)
	
	# parse input files and/or metaphlan files from commandline
	try:
		options, x = parser.parse_args(argv)
	except:
		parser.error('[FATAL]: parsing failure.\n')
		exit(1)
	
	if not options.outdir:
		parser.error('[FATAL]: output directory is not supplied, you must specify output prefix by -o/--outdir.\n')
		exit(1)
	
	if not os.path.exists(options.outdir): os.mkdir(options.outdir)	
	# preserve proj info
	projInfo_pkl = '%s/proj_info.pkl' % options.outdir
	if os.path.exists(projInfo_pkl):
		samples, assemblies, coords_files = cPickle.load(open(projInfo_pkl, 'rb'))
	else:
		samples, assemblies, coords_files = parse_config(options.config)
		cPickle.dump([samples, assemblies, coords_files], open(projInfo_pkl, 'wb'))
	
	project_construct(samples, assemblies, coords_files, options.outdir,
			identity_thr = options.min_identity, aln_len_thr = options.min_len,
			 verbose = options.verbose)
	metaseq_batch_init(samples, options.outdir, identity_thr = options.min_identity, 
					aln_len_thr = options.min_len, verbose = options.verbose)
	
	
if __name__ == '__main__': main()
	
	