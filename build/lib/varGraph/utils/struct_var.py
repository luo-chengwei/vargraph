#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

"""The utility script for generate the representative sequences for a cohort configured in
a configuration file.

This script is part of the varGraph package under MIT license.
"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

USAGE = \
"""Usage: 
%prog [options] -p/--proj <varGraph project directory> -c/--config <sample config file>

The sample config file should follow the format:

sample_1	group_A
sample_2	group_B
sample_3	group_A
sample_4	group_C
sample_5	group_B
sample_6	group_B
......
sample_N	group_C

The sample_x IDs should match the sample IDs used in project configuration.

Add --help to see a full list of required and optional
arguments to run the script.

Additional information can also be found at:
https://bitbucket.org/luo-chengwei/vargraph/wiki


varGraph: expanding variation analysis in series metagenomes

If you use varGraph in your work, please cite it as:
<varGraph citation here>

Copyright: Chengwei Luo, Georgia Institute of Technology, 2016.
"""

import sys, os, re, glob, cPickle, glob
from operator import itemgetter
from optparse import OptionParser, OptionGroup
from itertools import combinations
#from subprocess import call, Popen, PIPE
#import multiprocessing as mp

from varGraph import *


################################### FUNCTIONS ######################################
def get_struct_var(proj_dir, s1, s2, len_thr=500, verbose=False):
	"""generate the representative sequences for a cohort.
	
	Parameters
	----------
	proj_dir: varGraph project directory. It can be initiated by algorithms.vargraph_project.project_construct()
	s1: sample set 1
	s2: sample set 2
	
	Optional parameters
	-------------------
	len_thr: the minimum length to be used in this struct variation generation
	verbose: Default to be false. If turned on, prints out runtime information to help debug.
	
	"""
	metaseq_dir = '%s/MetaSeqs/' % proj_dir
	if not os.path.exists(metaseq_dir):
		sys.stderr.write('[FATAL] Cannot locate the MetaSeq directory that is supposed to hold the batch PKLs of MetaSeq objs.\n')
		exit(1)
	
	metaseq_pkls = glob.glob('%s/*.pkl' % metaseq_dir)
	if metaseq_pkls == None:
		sys.stderr.write('[FATAL] The MetaSeq directory appears to have zero MetaSeq pkl.\n')
		exit(1)
	
	node_dict_pkl = '%s/node_dict.pkl' % proj_dir
	if not os.path.exists(node_dict_pkl):
		sys.stderr.write('[FATAL] Cannot locate node_dict.pkl in the varGraph directory you supplied.\n')
		exit(1)
	
	# load node_dict
	if verbose: sys.stdout.write('[get_struct_var] Loading node dict.\n')
	node_dict = cPickle.load(open(node_dict_pkl, 'rb'))
	if verbose: sys.stdout.write('[get_struct_var] Loaded node dict.\n')
	for metaseq_pkl in metaseq_pkls:
		struct_vars = []
		ind = int(re.search('batch\_(\d+).pkl', os.path.basename(metaseq_pkl)).group(1))
		meta_seqs = cPickle.load(open(metaseq_pkl, 'rb'))
		if verbose: sys.stdout.write('[get_struct_var] Batch #%i loaded.\n' % (ind))
		for meta_seq_tag in meta_seqs:
			meta_seq=meta_seqs[meta_seq_tag]
			SVs = meta_seq.get_struct_vars(s1, s2, len_thr=len_thr)
			if len(SVs) == 0: continue
			for sv in SVs: struct_vars.append([meta_seq_tag, sv])
		for tag, sv in struct_vars:
			print tag
			for K in sv.coords: print K, sv.coords[K]
		if verbose: sys.stdout.write('[get_struct_var] Batch #%i finished.\n' % (ind))	
	
		
	return 0


################################### MAIN ######################################
def main(argv = sys.argv[1:]):

	parser = OptionParser(usage = USAGE, version="Version: " + varGraph.__version__)

	# Compulsory arguments
	compOptions = OptionGroup(parser, "Compulsory parameters",
						"There options are compulsory, and may be supplied in any order.")
	
	compOptions.add_option("-p", "--proj", type = "string", metavar = "DIR",
							help = "The varGraph project directory.")
	
	compOptions.add_option("-c", "--config", type = "string", metavar = "FILE",
							help = "The sample configuration file.")
	
	parser.add_option_group(compOptions)

	# Optional arguments that need to be supplied if not the same as default
	optOptions = OptionGroup(parser, "Optional parameters",
						"There options are optional, and may be supplied in any order.")

	
	optOptions.add_option("-l", "--len_thr", type = "int", default = 500, metavar = "INT",
							help = "The minimum length requirement for identifying structural variation. [default: 500]")
	
	optOptions.add_option("--verbose", default = False, action = "store_true",
							help = "Print out runtime information, helpful in debug, \
							otherwise, only important warnings/errors will show [default: False].")
								
	parser.add_option_group(optOptions)
	
							
	# parse input files and/or metaphlan files from commandline
	try:
		options, x = parser.parse_args(argv)
	except:
		parser.error('[FATAL]: parsing failure.\n')
		exit(1)
	
	if not options.proj:
		parser.error('[FATAL]: output directory is not supplied, you must specify output prefix by -p/--proj.\n')
		exit(1)
	
	if not options.config or not os.path.exists(options.config):
		parser.error('[FATAL]: sample configuration file is not supplied, you must specify the configuration file by -c/--config.\n')
		exit(1)
	
	if options.len_thr < 100:
		sys.stderr.write('[WARNING]: Cannot have len_thr smaller than 100, will use 500bp instead.\n')
		options.len_thr = 500
	
	proj_info_pkl = '%s/proj_info.pkl' % options.proj
	node_dict_pkl = '%s/node_dict.pkl' % options.proj
	if not os.path.exists(proj_info_pkl):
		sys.stderr.write('[FATAL] cannot locate proj_info.pkl in the varGraph project directory you supplied.\n')
		exit(1)
		
	# load basic info
	samples, assemblies, coords_files = cPickle.load(open(proj_info_pkl, 'rb'))
	# parse sample config
	sample_groups = {}
	for line in open(options.config, 'rb'):
		s, g = line.rstrip().split('\t')
		if s not in samples:
			sys.stderr.write('[FATAL] Sample %s supplied in sample config file does not exists in this varGraph project.\n' % s)
			exit()
		if g not in sample_groups: sample_groups[g] = []
		sample_groups[g].append(s)

	if len(sample_groups) < 2:
		sys.stderr.write('[FATAL] Less than 2 groups of samples, cannot compare structure between groups.\n')
		exit()

	# struct_var directory
	struct_var_dir = '%s/struct_var/' % options.proj
	if not os.path.exists(struct_var_dir): os.mkdir(struct_var_dir)
	
	# comparisons generation
	comps = []
	for g1, g2 in list(combinations(sample_groups.keys(), 2)):
		s1 = sample_groups[g1]
		s2 = sample_groups[g2]
		comps.append([s1, s2])
	
	get_struct_var(options.proj, s1, s2, len_thr=options.len_thr, verbose=options.verbose)
	
	
	
			
if __name__ == '__main__': main()
