#!/usr/bin/env/python
# -*- coding: utf-8 -*- 

"""The utility script for varGraph project construction for a cohort configured in
a configuration file.

This script is part of the varGraph package under MIT license.
"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

USAGE = \
"""Usage: 
%prog [options] -c/--conf <config.file> -o/--outprefix <output prefix>

The config file should follow the format:

[fa]
sample_1	/dir/to/sample_1/assembly/fasta/file   # tab-delimited
..... # You can add as many samples as you wish
sample_N	/dir/to/sample_N/assembly/fasta/file
[coords]
sample_1	sample_2	/dir/to/sample_1_vs_sample_2_coords_file 
sample_1	sample_3	/dir/to/sample_1_vs_sample_3_coords_file 
...
sample_N-1	sample_N	/dir/to/sample_N-1_vs_sample_N_coords_file 


Add --help to see a full list of required and optional
arguments to run the script.

Additional information can also be found at:
https://bitbucket.org/luo-chengwei/vargraph/wiki


varGraph: expanding variation analysis in series metagenomes

If you use varGraph in your work, please cite it as:
<varGraph citation here>

Copyright: Chengwei Luo, Georgia Institute of Technology, 2016.
"""

import sys, os, re, glob, cPickle
import random
import sqlite3
from optparse import OptionParser, OptionGroup
from itertools import combinations

from varGraph.algorithms import nucmer_init
from varGraph import *

################################### FUNCTIONS ######################################

def parse_config(config_file):
	assemblies = {}
	coords_files = {}
	flag = 0
	try:
		for line in open(config_file, 'rb'):
			if line[:-1] == '[fa]': flag = 1; continue
			if line[:-1] == '[coords]': flag = 2; continue
			if flag == 1:
				try: sample, fa = line[:-1].split('\t')
				except:raise Exception('Error in parsing assembly line.')
				if not os.path.exists(fa):
					raise Exception('Error in parsing assembly file.')
				assemblies[sample] = fa
			elif flag == 2:
				try: s1, s2, f = line[:-1].split('\t')
				except: raise Exception('Error in parsing coords file line.')
				if not os.path.exists(f):
					raise Exception('Error in parsing coords file.')
				coords_files[(s1, s2)] = f
			else:
				raise Exception('Error in parsing config file.')
	except Exception as e:
		sys.stderr.write('[FATAL]: %s.\n' % e)
		exit(1)
	samples = assemblies.keys()
	for s1, s2 in list(combinations(samples, 2)):
		if (s1, s2) not in coords_files and (s2, s1) not in coords_files:
			sys.stderr.write('[FATAL]: cannot locate coords file for %s vs %s.\n' % (s1, s2))
	return samples, assemblies, coords_files


def proj_construct(samples, assemblies, coords_files, outdir, verbose = False):
	sqlite_db = '%s/proj_db.sqlite' % outdir
	metaseq_dir = '%s/MetaSeqs/' % outdir
	node_dict_pkl = '%s/node_dict.pkl' % outdir
	sqlite_db = '%s/proj_db.sqlite' % outdir
	metaseq_dir = '%s/MetaSeqs/' % outdir
	node_dict_pkl = '%s/node_dict.pkl' % outdir
	if not os.path.exists(outdir):
		try: os.mkdir(outdir)
		except:
			sys.stderr.write('[FATAL] Cannot create the output directory you supplied: %s\n' % (outdir))
			exit(1)
			
	if not os.path.exists(node_dict_pkl):
		metaseq_pkls, node_dict_pkl = nucmer_init.init_from_nucmer(samples, assemblies, \
						coords_files, outdir, verbose = verbose)
	if verbose: sys.stdout.write('MetaSeq objects initiated!\n')

################################### MAIN ######################################
def main(argv = sys.argv[1:]):

	parser = OptionParser(usage = USAGE, version="Version: " + varGraph.__version__)

	# Compulsory arguments
	compOptions = OptionGroup(parser, "Compulsory parameters",
						"There options are compulsory, and may be supplied in any order.")
	
	compOptions.add_option("-o", "--outdir", type = "string", metavar = "DIR",
							help = "The output directory.")
	
	compOptions.add_option("-c", "--config", type = "string", metavar = "FILE",
							help = "The configuration file of the varGraph rep_seq project.")
	
	parser.add_option_group(compOptions)
	
	
	# Optional arguments that need to be supplied if not the same as default
	optOptions = OptionGroup(parser, "Optional parameters",
						"There options are optional, and may be supplied in any order.")
							
	optOptions.add_option("--verbose", default = False, action = "store_true",
							help = "Print out runtime information, helpful in debug, \
							otherwise, only important warnings/errors will show [default: False].")
								
	parser.add_option_group(optOptions)
	
	# parse input files and/or metaphlan files from commandline
	try:
		options, x = parser.parse_args(argv)
	except:
		parser.error('[FATAL]: parsing failure.\n')
		exit(1)
	
	if not options.outdir:
		parser.error('[FATAL]: output directory is not supplied, you must specify output prefix by -o/--outdir.\n')
		exit(1)
		
	samples, assemblies, coords_files = parse_config(options.config)
	proj_construct(samples, assemblies, coords_files, options.outdir, verbose = options.verbose)
	
if __name__ == '__main__': main()
	
	