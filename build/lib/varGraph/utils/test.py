import sys, re, os, glob
import cPickle

metaseq = cPickle.load(open(sys.argv[1], 'rb'))
metaseq.to_GFA(sys.argv[2])
for seq, path, X, gfa_strings in metaseq.get_rep_seqs(): print seq