# -*- coding: utf-8 -*-
"""
**********
Exceptions
**********

Base exceptions and errors for varGraph.

"""
import release

__author__ = release.authors

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <cluo@sequanacomp.com>
#    All rights reserved.
#    MIT license.


# Exception handling

# the root of all Exceptions
class varGraphException(Exception):
    """Base class for exceptions in varGraph."""

class varGraphError(varGraphException):
    """Exception for a serious error in varGraph"""
