"""Base class for initialize varGraph from nucmer alignment files.
"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

import sys, os, re, random
import cPickle
import networkx as nx
from Bio import SeqIO
from itertools import combinations

from varGraph.classes.metaseq import MetaSeq
from varGraph.classes.seqnode import SeqNode
from varGraph.classes.aln import AlnDetails
from varGraph.algorithms.sql import fasta2sql, batch_query_seq


def gen_subgraphs(samples, fasta_dict, nucmer_dict, identity_thr, aln_len_thr, verbose=False):
	"""The basic operations to initiate use nucmer alignment to guide contigs grouping.
	
	The Nucmer alignment should be created by Nucmer's coords algorithm.
	
	Parameters
	----------
	samples: the names of the samples in a list or tuple
	fasta_dict: a dict keyed by the samples IDs in the samples list, and valued by the path
				to the fasta file of the corresponding sample
	nucmer_dict: a dict keyed by the tuple of the sample IDs in comparison, and valued by
				the path to the coords files of the nucmer alignment.			
		
	Optional Parameters
	-------------------
	identity_thr: the nucleotide identity lower bound threshold for an alignment to be included.
					The default is 97.
	aln_len_thr: the alignment length lower bound threshold for an alignment to be included.
					The default is 500.
	verbose: if set True, then print out runtime information. The default is False.
				
	Returns
	-------
	subgraphs: a list of subgraphs that could be used to init MetaSeqs (varGraphs) objects.
	
	"""
	
	# load sequences
	G = nx.MultiGraph()
	seq_ind = 0
	for sample in samples:
		try:
			for rec in SeqIO.parse(fasta_dict[sample], 'fasta'):
				seq_ind+=1
				G.add_node((sample, rec.description), seq_ind = seq_ind, seqnode=None)
		except ValueError:
			sys.stderr.write('Error in processing the fasta sequence file for sample: %s.\n' % (sample))
			exit(1)
	if verbose:
		sys.stdout.write('[algorithms::nucmer_init::gen_subgraphs] %i nodes added to grand graph.\n' % (G.number_of_nodes()))
		
	# load alignments	
	for sample1, sample2 in list(combinations(samples, 2)):
		nucmer_file = None
		if (sample1, sample2) in nucmer_dict: 
			nucmer_file=nucmer_dict[(sample1, sample2)]
		elif (sample2, sample1) in nucmer_dict: 
			nucmer_file=nucmer_dict[(sample2, sample1)]
		else:
			sys.stderr.write('Error in locating the nucmer coords file for sample pair: %s and %s.\n' % (sample1, sample2))
			exit(1)
		ifh = open(nucmer_file, 'rb')
		for f in range(5): ifh.readline()
		edges = []
		while 1:
			line = ifh.readline().rstrip()
			if not line: break
			cols = [x for x in line.split() if x != '' and x != '|']
			s1, e1, s2, e2, aln_len1, aln_len2 = [int(s) for s in cols[:6]]
			identity = float(cols[6])
			len1, len2 = [int(s) for s in cols[7:9]]
			cov1, cov2 = [float(s) for s in cols[9:11]]
			tag1, tag2 = cols[-2:]
			if identity < identity_thr or min(aln_len1, aln_len2) < aln_len_thr: continue
			if s1 > e1: s1, e1 = e1, s1; strand1 = '-'
			else: strand1 = '+'
			if s2 > e2: s2, e2 = e2, s2; strand2 = '-'
			else: strand2 = '+'
			A = AlnDetails()
			A.ids = [(sample1, tag1), (sample2, tag2)]
			A.coords = [(s1-1, e1, strand1), (s2-1, e2, strand2)]
			A.identity = identity
			edges.append([(sample1, tag1), (sample2, tag2), A])
		ifh.close()
		
		# test if need to reverse the samples
		reversed = False
		for (sample1, tag1), (sample2, tag2), A in edges[:100]:
			if (sample1, tag1) not in G and (sample2, tag1) in G:
				reversed = True
				break
		for	(sample1, tag1), (sample2, tag2), A in edges:
			if reversed:
				node1 = (sample2, tag1)
				node2 = (sample1, tag2)
				A.ids = [(sample2, tag1), (sample1, tag2)]
			else:
				node1 = (sample1, tag1)
				node2 = (sample2, tag2)
			G.add_edge(node1, node2, aln=A)
	
	# get subgraphs for metaSeq init
	subgraphs = list(nx.connected_component_subgraphs(G))
	if verbose: sys.stdout.write('[algorithms::nucmer_init::gen_subgraphs] %i subgraphs generated.\n' % len(subgraphs))
	
	return subgraphs

def init_from_nucmer(samples, fasta_dict, nucmer_dict, outdir, \
				identity_thr = 97, aln_len_thr = 500, verbose=False):
	"""The basic operations to initiate the varGraph object from nucmer alignments; this is suitable
	for smaller projects as it operates entires in RAM. For large projects, use:
	init_from_nucmer_large()
	
	The Nucmer alignment should be created by Nucmer's coords algorithm.
	
	Parameters
	----------
	samples: the names of the samples in a list or tuple
	fasta_dict: a dict keyed by the samples IDs in the samples list, and valued by the path
				to the fasta file of the corresponding sample
	nucmer_dict: a dict keyed by the tuple of the sample IDs in comparison, and valued by
				the path to the coords files of the nucmer alignment.			
	outdir: a directory that the output pickles are stored at, it has to be writable.
		
	Optional Parameters
	-------------------
	identity_thr: the nucleotide identity lower bound threshold for an alignment to be included.
					The default is 97.
	aln_len_thr: the alignment length lower bound threshold for an alignment to be included.
					The default is 500.
	verbose: if set True, then print out runtime information. The default is False.
	
	
	Example
	----------
	In this test.py we show how to use it for 3 samples in Lanier cohort to construct 
	varGraph objects for the contigs:
	
	test.py:
		!# /usr/bin/env python
		
		import sys, re, cPickle, os
		from varGraph.algorithms import nucmer_init

		samples = ['20090826', '20090828', '20090907']
		fasta_dict = {}
		for s in samples:
			fasta_dict[s] = '/Volumes/Adam/Projects/Lanier/assemblies/%s.500+.fa' % s

		from itertools import combinations, product
		nucmer_dict = {}
		for s1, s2 in list(combinations(samples, 2)):
		f1 = 'Projects/Lanier/metaHGT/nucmer/%s.vs.%s.coords' % (s1, s2)
		f2 = 'Projects/Lanier/metaHGT/nucmer/%s.vs.%s.coords' % (s2, s1)
		if os.path.exists(f1):nucmer_dict[(s1, s2)] = f1
		else:nucmer_dict[(s2, s1)] = f2
		init_from_nucmer(samples, fasta_dict, nucmer_dict, outdir)
	
	"""
	
	if not os.path.exists(outdir):
		try: os.mkdir(outdir)
		except: 
			sys.stderr.write('[FATAL] Cannot create the outdir for rep_seq generation.\n')
			exit(1)
		
	# load all the assemblies to sqlite db
	sqlite_db = '%s/proj_db.sqlite' % outdir
	if not os.path.exists(sqlite_db):
		fasta2sql(samples, fasta_dict, sqlite_db, verbose=verbose)
	
	subgraphs_pkl = '%s/subgraphs.pkl' % outdir
	if not os.path.exists(subgraphs_pkl):
		subgraphs = gen_subgraphs(samples, fasta_dict, nucmer_dict, identity_thr, aln_len_thr, verbose=verbose)
		cPickle.dump(subgraphs, open(subgraphs_pkl, 'wb'))
		sys.stdout.write('[algorithms::nucmer_init::init_from_nucmer] %i subgraphs for MetaSeq objects construction found.\n' % (len(subgraphs)))
	
	return 0
	
	

def chunks(l, n):
	"""Yield successive n-sized chunks from l."""
	for i in range(0, len(l), n):
		yield l[i:i+n]

