

def msa(sequences=[]):
	sSequences = []
	sSequences2 = []
	scores = []
	
	
	for sequence2 in sequences2:
		for sequence1 in sequences1:
			aSequence1, aSequence2 = alignSequences(sequence1, sequence2)
			score = alignScore(aSequence1, aSequence2)
			if scores == []:
				sSequences1.append(aSequence1)
				sSequences2.append(aSequence2)
				scores.append(score)
			else:
				# Sort sequences by score.
				imin = 0
				imax = len(scores) - 1
				imid = (imax + imin) >> 1
				while True:
					if score == scores[imid]:
						sSequences1 = sSequences1[: imid + 1] + \
										[aSequence1] + \
										sSequences1[imid + 1:]
						sSequences2 = sSequences2[: imid + 1] + \
										[aSequence2] + \
										sSequences2[imid + 1:]
						scores = scores[: imid + 1] + \
									[score] + scores[imid + 1:]
						break
					elif score < scores[imid]: imax = imid
					elif score > scores[imid]: imin = imid
					imid = (imax + imin) >> 1
					if imid == imin or imid == imax:
						if score < scores[imin]:
							sSequences1 = sSequences1[: imin] + [aSequence1] + sSequences1[imin:]
							sSequences2 = sSequences2[: imin] + [aSequence2] + sSequences2[imin:]
							scores = scores[: imin] + [score] + scores[imin:]
						elif score > scores[imax]:
							sSequences1 = sSequences1[: imax + 1] + [aSequence1] + sSequences1[imax + 1:]
							sSequences2 = sSequences2[: imax + 1] + [aSequence2] + sSequences2[imax + 1:]
							scores = scores[: imax + 1] + [score] + scores[imax + 1:]
						else:
							sSequences1 = sSequences1[: imin + 1] + [aSequence1] + sSequences1[imin + 1:]
							sSequences2 = sSequences2[: imin + 1] + [aSequence2] + sSequences2[imin + 1:]
							scores = scores[: imin + 1] + [score] + scores[imin + 1:]
						break