"""Base class for initialize SQLite DB for project contigs.
"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

import sys, os, re, random
import sqlite3
from Bio import SeqIO

def fasta2sql(samples, fasta_dict, sqlite_db, verbose = False):
	"""Initialize a SQLite DB for the project contigs
	
	Parameters
	----------
	samples: a list of sample names to construct the SQLite DB
	fasta_dict: a dict object keyed by sample names, valued by directory to fasta file of contigs
	sqlite_db: disk location to the sqlite DB to be constructed
	
	Optional Parameters
	-------------------
	verbose: prints out status to stdout
	"""
	seq_table = 'all_seqs'
	sql_conn = sqlite3.connect(sqlite_db)
	c = sql_conn.cursor()
	c.execute("CREATE TABLE {tn} ({fn1} INTEGER PRIMARY KEY, \
				{fn2} TEXT, {fn3} TEXT, {fn4} TEXT)".format(tn=seq_table, \
				fn1='seq_ind', fn2='sample', fn3='tag', fn4='sequence'))
	sql_conn.commit()
	
	if verbose:
			sys.stdout.write('[algorithms::sql::fasta2sql] all_seqs table made.\n')
			
	seq_ind = 0
	for sample in samples:
		N = 0
		try:
			for rec in SeqIO.parse(fasta_dict[sample], 'fasta'):
				seq_ind+=1
				tag = rec.description
				seq = str(rec.seq)
				c.execute("INSERT INTO all_seqs (seq_ind, sample, tag, sequence) \
							VALUES ({ind}, '{sample}', '{tg}', '{sq}')"\
				.format(ind=seq_ind, sample = sample, tg=tag, sq=seq))
				N+=1
		except ValueError:
			sys.stderr.write('Error in processing the fasta sequence file for sample: %s.\n' % (sample))
			exit(1)
		if verbose:
			sys.stdout.write('[algorithms::sql::fasta2sql] %i queries added from sample: %s.\n' % (N, sample))
		
	sql_conn.commit()
	sql_conn.close()
	if verbose:
		sys.stdout.write('[algorithms::sql::fasta2sql] %i queries added from %i samples.\n' % (seq_ind, len(samples)))
	return 0
	
			
def batch_query_seq(sqlite_db, queries, verbose=False):
	"""Query a SQLite DB using a list of sequence's index IDs
	It returns the sequences
	
	Parameters
	----------
	sqlite_db: the location of the SQLite DB
	queries: list of INT type primary keys of query sequences
	
	Optional Parameters
	-------------------
	verbose: prints out status to stdout
	
	Returns
	-------
	sequence_records: dict keyed by seq record index, valued by tuples in 
				format (sample, seq_description, sequence)
	
	"""
	conn = sqlite3.connect(sqlite_db)
	cursor = conn.cursor()
	sequences = {}
	for seq_ind in queries:
		cursor.execute("SELECT * FROM all_seqs WHERE seq_ind={ind}".format(ind=seq_ind))
		s = cursor.fetchone()
		sequences[seq_ind] = tuple(s[1:])
	if verbose: sys.stdout.write('[algorithms::sql::batch_query_seq] %i sequences fectched.\n' % len(sequences))
	conn.close()
	return sequences