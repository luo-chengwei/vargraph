"""Release data for varGraph."""


#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.


from __future__ import absolute_import

import os
import sys
import time
import datetime

basedir = os.path.abspath(os.path.split(__file__)[0])

def get_info():
	## Date information
	date_info = datetime.datetime.now()
	date = time.asctime(date_info.timetuple())
	revision, version, version_info, vcs_info = None, None, None, None
	version=''.join([str(major), '.', str(minor)])
	version_info = (name, major, minor, revision)
	return date, date_info, version, version_info

## Version information
name = 'varGraph'
major = '0'
minor = '1'


## Declare current release as a development release.
## Change to False before tagging a release; then change back.
dev = True

description = "Python package for creating and manipulating series metagenomic data in graphs"

long_description = \
"""varGraph (VG) is a Python package for the creation, manipulation, and
study of the population diversity for metagenomics and beyond.	
"""
license = 'MIT'
authors = {'Chengwei' : ('Chengwei Luo','luo.chengwei@gatech.edu'),
           'Miguel' : ('Luis Miguel Rodriguez-R.','lrr@gatech.edu'),
           'Kostas' : ('Konstantinidis T. Konstantinidis','kostas.konstantinidis@gatech.edu')
           }

url = 'http://www.bitbucket.org/luo-chengwei/vargraph/'
platforms = ['Linux','Mac OSX','Windows','Unix']
keywords = ['Graph Theory', 'Assembly', 'Metagenomics', 'graph', 'sequence']

classifiers = [
        'Development Status :: Dev',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Topic :: Scientific/Engineering :: Information Analysis']

date, date_info, version, version_info = get_info()

