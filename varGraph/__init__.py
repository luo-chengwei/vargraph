"""
varGraph
========
	varGraph (VG) is a Python package for the creation, manipulation, and
	study of the population diversity for metagenomics and beyond.
	
	https://www.bitbucket.org/luo-chengwei/vargraph

Using
-----
    Just write in Python

    >>> import vargraph as vg
    >>> G=vg.varGraph()
    >>> G.add_seq({'a':'ACTTGGATGATGAAGTA'}, {'b':'ACGTGGATGA'})
    >>> G.alignSeq()
    >>> unitigs=G.unitig(nt_identity=90)
    >>> print unitigs
    [('unitig_1', 'ACTTGGATGATGAAGTA')]
    >>> unitigs=G.unitig()
    [('unitig_1', 'ACTTGGATGATGAAGTA'), ('unitig_2', 'ACGTGGATGAAGTA')]
"""


#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.
#
# Add platform dependent shared library path to sys.path
#

from __future__ import absolute_import
import sys

if sys.version_info[:2] < (2, 7):
    m = "Python 2.7 or later is required for varGraph (%d.%d detected)."
    raise ImportError(m % sys.version_info[:2])
del sys

# Release data
from varGraph import release

__author__ = '%s <%s>\n%s <%s>\n%s <%s>' % \
    (release.authors['Chengwei'] +
		release.authors['Miguel'] +
		release.authors['Kostas'])
__license__ = release.license

__date__ = release.date
__version__ = release.version

# These are import orderwise
from varGraph.exception import *

import varGraph.classes
from varGraph.classes import *

