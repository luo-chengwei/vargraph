"""Base class for contig sequence graph.

The MetaSeq class allows manipulations on variation graphs.

"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

import random, operator, string
from itertools import product, combinations	
import networkx as nx
from Bio import SeqIO, Seq
from varGraph.classes.seqnode import SeqNode
from varGraph.classes.aln import AlnDetails
from varGraph.classes.structvar import StructVar

translate_table = string.maketrans('ATGCatgc', 'TACGtacg')

def rc_seq(seq):
	"""The functions that make the reverse complementary sequence of a given input sequence
	
	Parameters
	----------
	seq: string type sequence
	
	Returns
	-------
	string: the reverse complementary sequence of input sequence
	"""
	return str(seq[::-1]).translate(translate_table)
	
def get_GFA_strings(X):
	"""The function that returns GFA file in a list of strings that could be directly piped
	into a file
	
	Parameters
	----------
	X: the nx.DiGraph type of a metaseq object
	
	Returns
	-------
	gfa_strings: list of strings that are lines in the final GFA file
	
	Note
	----
	the header line is not included.
	"""
	node_ind = 0
	node_dict = {}
	gfa_lines = []
	for n1, n2, e in X.edges(data=True):
		if 'seq' not in e or e['seq'] == '': continue
		node_ind+=1
		node_dict[node_ind] = [n1, n2]
		seq = e['seq']
		gfa_lines.append('S\t%i\t%s' % (node_ind, seq))
	# get link lines
	for n1, n2, e in X.edges(data=True):
		if 'aln' in e:
			f1 = [0, -1]
			f2 = [0,-1]
			for node_ind in node_dict:
				x, y = node_dict[node_ind]
				if x == n1: f1=[node_ind, 0]
				if x == n2: f2=[node_ind, 0]
				if y == n1: f1=[node_ind, 1]
				if y == n2: f2=[node_ind, 1]
			if f1[1] == 0 and f2[1] == 0: o1, o2 = '-', '+'
			elif f1[1] == 0 and f2[1] == 1: o1, o2 = '-', '-'
			elif f1[1] == 1 and f2[1] == 0: o1, o2 = '+', '+'
			elif f1[1] == 1 and f2[1] == 1: o1, o2 = '+', '-'
			else: continue
			gfa_lines.append('L\t%i\t%s\t%i\t%s\t0M' % (f1[0], o1, f2[0], o2))
	
		elif 'seq' in e and e['seq'] == '':
			f1 = 0
			f2 = 0
			for node_ind in node_dict:
				x, y = node_dict[node_ind]
				if y == n1: f1=node_ind
				if x == n2: f2=node_ind
			gfa_lines.append('L\t%i\t+\t%i\t+\t0M' % (f1, f2))
	return gfa_lines
		
class MetaSeq(nx.MultiDiGraph):
	"""Base class for meta-sequence operations
	
	Itself is a NetworkX.DiGraph object, with nodes being SeqNode object (SeqNode.uid actually),
	and edges being two types: 1) contig connection; and 2) alignment, with alignment 
	details specified in an AlnDetails object
	
	Parameters
	----------
	
	Examples
	---------
	"""

	def __init__(self):
		"""
		Initialize a MetaSeq object with graph, SeqNodes, and node_lookup/dict
		
		Parameters
		----------
		self: input DiGraph
		G: a DiGraph that keep the updated graph
		contigs: keeps traversal objects to define contigs
		"""
		nx.MultiDiGraph.__init__(self)
		self.G = nx.DiGraph()
		self.contigs = {}
		self.alns = {}
		
	def total_length(self):
		L = 0
		for node, node_details in self.nodes(data=True):
			seqnode = node_details['seqnode']
			L+=seqnode.get_length()
		return L
		
	def __gen_IDs(self, avoid = [], N = 1):
		IDs = []
		A = avoid
		while len(IDs) < N:
			uid = random.randint(1, 100000)
			if uid not in A:
				A.append(uid)
				IDs.append(uid)
		return IDs
			
	def add_seq(self, sNode):
		"""add single SeqNode object to MetaSeq object
		
		Parameters
		----------
		sNode: single SeqNode object
		"""
		ID = self.__gen_IDs(avoid=self.nodes())[0]
		self.add_node(ID, seqnode=sNode)
		return 0
	
	def add_seqs_from(self, sNodes):
		"""add SeqNodes in a batch mode
		
		Parameters:
		----------
		sNodes: list or tuple of SeqNode objects
		"""
		for sNode in sNodes: self.add_seq(sNode)
		return 0
	
	def __find_node(self, x):
		for node, node_details in self.nodes(data=True):
			sNode = node_details['seqnode']
			if tuple(sNode.origin) == x: return node
		return None
	
	def add_aln(self, aln):
		"""add AlnDetails object into MetaSeq object
		
		Parameters
		----------
		aln: AlnDetails object to be added
		"""
		
		id1, id2 = aln.ids
		seqnode1 = self.__find_node(id1)
		seqnode2 = self.__find_node(id2)
		(s1, e1, strand1), (s2, e2, strand2) = aln.coords
		self.node[seqnode1]['seqnode'].add_breakpoint(s1)
		self.node[seqnode1]['seqnode'].add_breakpoint(e1)
		self.node[seqnode2]['seqnode'].add_breakpoint(s2)
		self.node[seqnode2]['seqnode'].add_breakpoint(e2)
		#print seqnode1, seqnode2, s1, e1, strand1, s2, e2, strand2
		x1, y1 = (s1, '+'), (e1, '-')
		x2, y2 = (s2, '+'), (e2, '-')
		orientation = (strand1, strand2)
		identity = aln.identity
		if orientation == ('+','+'):
			self.add_edge(seqnode1, seqnode2, aln=[x1, x2], sister_aln = [y1, y2], identity=identity)
			self.add_edge(seqnode1, seqnode2, aln=[y1, y2], sister_aln = [x1, x2], identity=identity)
		elif orientation == ('+', '-'):
			self.add_edge(seqnode1, seqnode2, aln=[x1, y2], sister_aln = [y1, x2], identity=identity)
			self.add_edge(seqnode1, seqnode2, aln=[y1, x2], sister_aln = [x1, y2], identity=identity)
		
		return 0
	
	def add_aln_from(self, alns):
		"""batch add AlnDetails object into MetaSeq object
		
		Parameters
		----------
		alns: list of AlnDetails objects to be added
		"""
		for aln in alns: self.add_aln(aln)
		return 0
		
	def update_seq(self, samples = None):
		"""update the metaSeq object to a unified MultiDiGraph stored in self.G
		
		Optional Parameters
		-------------------
		samples: a list of samples so the updated graph only shows the ones from the
		        list of samples specified.
		"""
		if samples == None: samples = self.__get_all_samples()
		self.G = nx.MultiDiGraph()
		node_dict = {}
		for n in self.nodes():
			tag = self.node[n]['seqnode'].origin
			if tag[0] not in samples: continue
			node_dict[n] = tag
			S = self.node[n]['seqnode']
			for subn, subd in S.nodes(data=True):
				uniq_tag = tuple(tag+[subn])
				self.G.add_node(uniq_tag, subd)
			for subn1, subn2, e in S.edges(data=True):
				uniq_tag1 = tuple(tag+[subn1])
				uniq_tag2 = tuple(tag+[subn2])
				seq = e['seq']
				self.G.add_edge(uniq_tag1, uniq_tag2, seq=seq)
		
		# traverse G to find all the contigs
		self.contigs = {}
		subgraphs = list(nx.connected_component_subgraphs(self.G.to_undirected()))
		for subgraph in subgraphs:
			# every subgraph is a contig
			# find sNode and eNode
			contig_traversal = sorted([a for a in subgraph.nodes()], key=lambda t: (t[2][0], -ord(t[2][1])))
			contigID = tuple(contig_traversal[0][:2])
			self.contigs[contigID] = contig_traversal
		
		# add alignment information		
		for n1, n2, a in self.edges(data=True):
			try:
				tag1 = node_dict[n1]
				tag2 = node_dict[n2]
			except KeyError:
				continue
			x, y = a['aln']
			identity = a['identity']
			uniq_tag1 = tuple(tag1+[x])
			uniq_tag2 = tuple(tag2+[y])
			aln = a['aln']
			sister_aln = a['sister_aln']
			K = tuple(sorted([tuple(tag1), tuple(tag2)]))
			if K not in self.alns: self.alns[K] = []
			V1 = (tuple(aln), tuple(sister_aln))
			V2 = (tuple(sister_aln), tuple(aln))
			if V1 not in self.alns[K] and V2 not in self.alns[K]:
				self.alns[K].append(V1)
			self.G.add_edge(uniq_tag1, uniq_tag2, aln = aln, identity = identity)
			
		return 0
	
	def __get_all_samples(self):
		samples = set()
		for node in self.nodes():
			sample = self.node[node]['seqnode'].origin[0]
			samples.add(sample)
		return list(samples)
		
	def to_GFA(self, outprefix, samples=None):
		"""Output the metaSeq object graph into files
		
		There would be two files: [prefix].gfa and [prefix].node_dict.
		The .gfa file is the standard GFA representation of the varGraph;
		The .node_dict file is the dict file that provides details of the nodes in
		the .gfa file.
		
		Parameters
		----------
		outprefix: the prefix used in outfiles, e.g.: 
				output two files: [prefix].gfa and [prefix].node_dict.
		
		Optional Parameters
		-------------------
		samples: a list of samples so the GFA representation only shows the ones from the
		        list of samples specified.
		"""
		samples = self.update_seq(samples=samples)	
		
		header_line = 'H\tVN:Z:1.0'
		# write all the seq nodes
		seq_lines = []
		node_ind = 0
		node_dict = {}
		for n1, n2, e in self.G.edges(data=True):
			if 'seq' not in e or e['seq'] == '': continue
			if n1[0] not in samples: continue
			node_ind+=1
			node_dict[node_ind] = [n1, n2]
			seq = e['seq']
			seq_lines.append('S\t%i\t%s' % (node_ind, seq))
		# write all the links	
		link_lines = []
		for n1, n2, e in self.G.edges(data=True):
			if n1[0] not in samples or n2[0] not in samples: continue
			if 'aln' in e:
				f1 = [0, -1]
				f2 = [0, -1]
				for node_ind in node_dict:
					x, y = node_dict[node_ind]
					if x == n1: f1=[node_ind, 0]
					if x == n2: f2=[node_ind, 0]
					if y == n1: f1=[node_ind, 1]
					if y == n2: f2=[node_ind, 1]
				if f1[1] == 0 and f2[1] == 0: o1, o2 = '-', '+'
				elif f1[1] == 0 and f2[1] == 1: o1, o2 = '-', '-'
				elif f1[1] == 1 and f2[1] == 0: o1, o2 = '+', '+'
				elif f1[1] == 1 and f2[1] == 1: o1, o2 = '+', '-'
				else: print n1, n2, e
				link_lines.append('L\t%i\t%s\t%i\t%s\t0M' % (f1[0], o1, f2[0], o2))
			elif 'seq' in e and e['seq'] == '':
				f1 = 0
				f2 = 0
				for node_ind in node_dict:
					x, y = node_dict[node_ind]
					if y == n1: f1=node_ind
					if x == n2: f2=node_ind
				link_lines.append('L\t%i\t+\t%i\t+\t0M' % (f1, f2))
		gfh = open(outprefix+'.gfa', 'wb')
		gfh.write(header_line+'\n')
		gfh.write('\n'.join(seq_lines)+'\n')
		gfh.write('\n'.join(link_lines)+'\n')
		gfh.close()
		
		dfh = open(outprefix+'.node_dict', 'wb')
		dfh.write('# node_ID\tsample_ID\tsequence_ID\tseq_start_index\tseq_end_index\n')
		for node_ind in node_dict:
			x, y = node_dict[node_ind]
			sample, tag = x[:2]
			seq_ind1, seq_ori1 = x[2]
			seq_ind2, seq_ori2 = y[2]
			dfh.write('%i\t%s\t%s\t%i\t%i\n' % \
				(node_ind, sample, tag, seq_ind1, seq_ind2))
		dfh.close()
		
		return 0
	
	
	def cal_pNpS(self, gene_dict, samples = None):
		"""Calculate the pN/pS ratios for all the genes contained in this MetaSeq object
		
		Parameters
		----------
		gene_dict: A dict object keyed by (sample, contigID), valued by (start, end, strand)
		
		Optional Parameters
		-------------------
		samples: a list of samples so the representative sequences would only
				be generated using the specified samples. Default: all samples.
				
		Return
		------
		A list of all genes' pN/pS ratios
		
		"""
		if samples == None: samples = self.__get_all_samples()
		seqs = {}
		for contigID in self.contigs:
			contig_traversal = self.contigs[contigID]
			if contigID[0] not in samples: continue
			sequence = ''
			for sNode, eNode in zip(contig_traversal[:-1:2], contig_traversal[1::2]):
				try: seq = self.G[sNode][eNode][0]['seq']
				except: continue
				sequence+=seq
			seqs[contigID]=sequence
		print self.alns
	
	def get_struct_vars(self, samples1, samples2, len_thr = 500):
		"""Output the structural variations of the metaSeq object
		
		Parameters
		--------------
		samples1: a list of samples of group1
		samples2: a list of samples of group2
		
		Optional Parameters
		-------------------
		len_thr: the minimum length of a structure variation to be reported
		
		Returns
		--------
		SVs: a list of structure variations. Each entry is a StructVar object.
		
		"""
		seqs1 = []
		seqs2 = []
		S = set()
		for sample in samples1+samples2: S.add(sample)
		
		# load traversals, turn into segments
		segments1 = {}
		segments2 = {}
		contig_lengths = {}
		for contigID in self.contigs:
			contig_traversal = self.contigs[contigID]
			if contigID[0] not in S: continue
			if contigID[0] in samples1:
				if contigID not in segments1: segments1[contigID] = []
				for sNode, eNode in zip(contig_traversal[:-1:2], contig_traversal[1::2]):
					try: seq = self.G[sNode][eNode][0]['seq']
					except: continue
					start_coord = sNode[2][0]
					end_coord = eNode[2][0]
					segments1[contigID].append([start_coord, end_coord, seq])
					if contigID not in contig_lengths: contig_lengths[contigID] = 0
					contig_lengths[contigID]+=abs(end_coord-start_coord)
					
			elif contigID[0] in samples2:
				if contigID not in segments2: segments2[contigID] = []
				for sNode, eNode in zip(contig_traversal[:-1:2], contig_traversal[1::2]):
					try: seq = self.G[sNode][eNode][0]['seq']
					except: continue
					start_coord = sNode[2][0]
					end_coord = eNode[2][0]
					segments2[contigID].append([start_coord, end_coord, seq])
					if contigID not in contig_lengths: contig_lengths[contigID] = 0
					contig_lengths[contigID]+=abs(end_coord-start_coord)
		
		# scan for SVs
		SVs = []
		# traverse each seq-linked nodes, then find all the aln-linked nodes pairs
		X = nx.Graph() # hold all the nodes linked by aln-links
		for n1, n2, d in self.G.edges(data=True):
			if 'aln' in d: X.add_edge(n1, n2)
		for (sample, contig) in segments1:
			for s, e, seq in segments1[(sample, contig)]:
				sNode = (sample, contig, (s, '+'))
				eNode = (sample, contig, (e, '-'))
				X.add_edge(sNode, eNode)
		for (sample, contig) in segments2:
			for s, e, seq in segments2[(sample, contig)]:
				sNode = (sample, contig, (s, '+'))
				eNode = (sample, contig, (e, '-'))
				X.add_edge(sNode, eNode)
			
		# subgraphs of X would be segments linked by aln-links
		X_components = list(nx.connected_components(X))
		counts=[]
		agg_counts = [0,0]
		flag = 0
		candidates = []
		for nodes in X_components:
			# group by contigs
			segments = {}
			for sample, contig, coord in nodes:
				if (sample, contig) not in segments: segments[(sample, contig)]=[]
				segments[(sample, contig)].append((sample, contig, coord))
			count = [0, 0]
			xflag = 0
			for sample, contig in segments:
				c1 = segments[(sample, contig)][0][2][0]
				c2 = segments[(sample, contig)][1][2][0]
				L = abs(c1-c2)+1
				if L < len_thr: xflag=1; continue
				if c1 > c2: c1, c2 = c2, c1
				try: CL = contig_lengths[(sample, contig)]
				except: continue
				if c1 < 500 or c2+500 > CL: continue
				if sample in samples1: count[0]+=1; agg_counts[0]+=1
				elif sample in samples2: count[1]+=1; agg_counts[1]+=1
			if xflag == 1: continue
			if min(count) == 0:
				flag=1
				candidates.append(segments)
			counts.append(count)
		if min(agg_counts) < 3 or flag == 0: return []
		
		for segments in candidates:
			SV = StructVar()
			for sample, contig in segments:
				c1 = segments[(sample, contig)][0][2][0]
				c2 = segments[(sample, contig)][1][2][0]
				L = abs(c1-c2)+1
				if L < len_thr: continue
				if c1 > c2: c1, c2 = c2, c1
				try: CL = contig_lengths[(sample, contig)]
				except: continue
				if c1 < 500 or c2+500 > CL: continue
				K = (sample, contig)
				if K not in SV.coords: SV.coords[K]=[]
				SV.coords[K].append((c1, c2))
			if len(SV.coords)>0: SVs.append(SV)
		
		return SVs
	
	
	def get_rep_seqs(self, len_thr = 500, identity_thr=97, samples=None):
		"""Output the representative sequences of the metaSeq object
		
		Optional Parameters
		-------------------
		samples: a list of samples so the representative sequences would only
				be generated using the specified samples. Default: all samples.
		len_thr: the length low bound to output rep sequence. Default: 500bp.
		identity_thr: the nucleotide identity low bound. Default: 97%.
		
		Returns
		-------
		rep_seqs: a list of representative sequences, associated path, and subgraph
				 in format [sequence, seq_path, subgraph, gfa_strings]; in which the sequence is a 
				 string of the rep. seq, the seq_path is a list of nodes iterated to generate
				 such rep seq, subgraph is a nx.DiGraph object that holds the varGraph
				 that consists of the sequence; and gfa_strings is a list of strings that
				 could be used to write to a GFA file.
		"""
		if samples == None: samples = self.__get_all_samples()
		nt_thr = identity_thr
		# get the simplified graph in X with threshold thr
		X = nx.Graph()
		#X.add_nodes_from(self.G.nodes())
		# store sister alns
		for n1, n2, e in self.G.edges(data=True):
			if n1[0] not in samples or n2[0] not in samples: continue
			if 'aln' in e and e['identity'] >= nt_thr:
				X.add_edge(n1, n2, aln=e['aln'], aln_w = 1, seq_w = 0)
			elif 'seq' in e:
				X.add_edge(n1, n2, seq=[e['seq'], n1, n2], aln_w = 0, seq_w = 1)
		
		# orient the contigs first
		# randomly select one contig as reference direction
		direction_ref = self.contigs.keys()[0]
		D = nx.Graph()
		for c in self.contigs:
			if c[0] not in samples: continue
			D.add_node(c)
		
		A = {}
		for aln in self.alns:
			n1, n2 = aln
			if n1[0] not in samples or n2[0] not in samples: continue
			if aln not in A: A[aln] = [0, '+']
			(x1, y1), (x2, y2) = self.alns[aln][0]
			aln_len = abs(x1[0]-x2[0])
			if x1[0] < x2[0]: o1 = '+'
			else: o1 = '-'
			if y1[0] < y2[0]: o2 = '+'
			else: o2 = '-'
			if (o1, o2).count('-') % 2 == 0: o='+'
			else: o='-'
			if aln_len > A[aln][0]:
				A[aln]=[aln_len, o]
		
		for n1, n2 in A: 
			o = A[(n1, n2)][1]
			D.add_edge(n1, n2, ori=o)
		
		contig_orientations = {}
		for contig in self.contigs:
			if contig[0] not in samples: continue
			if contig == direction_ref: contig_orientations[contig] = '+'; continue
			try:p = nx.shortest_path(D, direction_ref, contig)
			except: contig_orientations[contig] = '+'; continue
			sigs = []
			for n1, n2 in zip(p[:-1], p[1:]): sigs.append(D[n1][n2]['ori'])
			if sigs.count('-') % 2 == 0: contig_orientations[contig] = '+'
			else: contig_orientations[contig] = '-'
			
		H = nx.DiGraph()
		# reverse those contigs with '-' orientation locally
		for contig in contig_orientations:
			if contig[0] not in samples: continue
			O = contig_orientations[contig]
			if O == '+':
				nodes = self.contigs[contig]
				for n1, n2 in zip(nodes[:-1], nodes[1:]):
					seq = X[n1][n2]['seq'][0]
					H.add_edge(n1, n2, seq=seq)
			elif O == '-':
				nodes = self.contigs[contig][::-1]
				for n1, n2 in zip(nodes[:-1], nodes[1:]):
					seq = rc_seq(X[n1][n2]['seq'][0])
					H.add_edge(n1, n2, seq=seq)
		# add alignment edges
		for aln in self.alns:
			n1, n2 = aln
			if n1[0] not in samples or n2[0] not in samples: continue
			for (x1, x2), (y1, y2) in self.alns[aln]:
				node11 = (n1[0], n1[1], x1)
				node12 = (n1[0], n1[1], y1)
				node21 = (n2[0], n2[1], x2)
				node22 = (n2[0], n2[1], y2)
				H.add_edge(node11, node21, seq='')
				H.add_edge(node21, node11, seq='')
				H.add_edge(node12, node22, seq='')
				H.add_edge(node22, node12, seq='')
		
		# generate representative sequences
		rep_seqs = []
		tip_nodes = []
		for contig in self.contigs:
			if contig[0] not in samples: continue
			nodes = self.contigs[contig]
			tip_nodes.append(nodes[0])
			tip_nodes.append(nodes[-1])
		tip_nodes = set(tip_nodes)
		while H.number_of_nodes() > 0:	
			#print H.nodes()
			nodes = list(nx.connected_components(H.to_undirected()))[0]
			S = H.subgraph(nodes)
			# make a graph in which the directions are reversed
			R = S.reverse(copy=True)		
			# use traversals on S to get representative sequences
			seed_node = random.sample(S.nodes(), 1)[0]
			# use DFS search in S
			reverse_seed = [seed_node, -1]
			for node in list(nx.dfs_preorder_nodes(S, source=seed_node)):
				try: p = nx.shortest_path(S, seed_node, node)
				except: continue
				seq_len = 0
				for n1, n2 in zip(p[:-1], p[1:]): seq_len+=len(S[n1][n2]['seq'])
				if seq_len > reverse_seed[1]: reverse_seed = [node, seq_len]
			reverse_seed_node = reverse_seed[0]
			# use DFS search in R
			target_seed = [reverse_seed_node, -1]
			for node in list(nx.dfs_preorder_nodes(R, source=reverse_seed_node)):
				p = nx.shortest_path(R, reverse_seed_node, node)
				seq_len = 0
				for n1, n2 in zip(p[:-1], p[1:]): seq_len+=len(R[n1][n2]['seq'])
				if seq_len > target_seed[1]: target_seed = [node, seq_len]
			source_node = target_seed[0]
			target_node = reverse_seed[0]
			target_path = nx.shortest_path(S, source_node, target_node)
			target_seq = ''
			for n1, n2 in zip(target_path[:-1], target_path[1:]):
				target_seq+=S[n1][n2]['seq']
			
			# get sequence, subgraph, GFA strings
			# recruit the associated nodes
			n1 = target_path[0]
			n2 = target_path[-1]
			associated_nodes = []
			for node in S.nodes():
				if node in target_path: associated_nodes.append(node); continue
				if nx.has_path(S, n1, node) and nx.has_path(S, node, n2):
					associated_nodes.append(node)
			H.remove_nodes_from(associated_nodes)
			X = self.G.subgraph(associated_nodes)
			gfa_strings = get_GFA_strings(X)
			if len(target_seq) >= len_thr:
				rep_seqs.append([target_seq, target_path, X, gfa_strings])
		
		return rep_seqs
		
def subgraph2metaSeq(subgraph):
	"""turn subgraphs into MetaSeq object
	
	Parameters
	----------
	subgraph: the nx.DiGraph type input object
	
	Returns
	-------
	MetaSeq: the metaseq object that is updated from input subgraph
	"""
	meta_seq = MetaSeq()
	breakpoints = {}
	# add nodes
	for node, node_detail in subgraph.nodes(data=True):
		seq_node = node_detail['seqnode']
		meta_seq.add_seq(seq_node)
	if subgraph.number_of_nodes() == 1:
		return meta_seq   # trivial case
	# add edges
	for node1, node2, edge_details in subgraph.edges(data=True):
		A = edge_details['aln']
		meta_seq.add_aln(A)
	return meta_seq
		
		