"""Base class for sequence nodes.

The SeqNode class allows any sequence to function as a node in 
the varGraph objects.
"""


#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

import string, random
import networkx as nx
from varGraph.exception import varGraphError

translate_table = string.maketrans('ATGCatgc', 'TACGtacg')

def rc_seq(seq):
	return str(seq[::-1]).translate(translate_table)

class SeqNode(nx.DiGraph):
	"""Base class for sequence nodes.

    A SeqNode stores sequences, the original sequence, the coordinates,
    orientation with optional data, or attributes.
    
    Parameters
    ----------
    origin: input contig's ID, the ID is a tuple, as [sample, contigID]
    uid: unique ID for this node
    start: the 1-offset start coordinate of this node's sequence of the origin sequence
    end: the 1-offset end coordinate of the this node's seuqence of the origin sequence
    rc_seq: the reverse complementary sequence
    coords: the tuple of [start, end]
    
	"""
	
	def __init__(self, sample, tag, seq):
		"""Initialize a SeqNode object with nx.DiGraph()
		
		Parameters
		----------
		sample: the SeqNode's sample
		tag: the SeqNode's ID in string type
		seq: the SeqNode's sequence in string
		"""
		nx.DiGraph.__init__(self)
		L = len(seq)
		sNode = (0, '+')
		eNode = (L, '-')
		self.origin = [sample, tag]
		self.tnodes=[sNode, eNode]
		self.add_node(sNode)
		self.add_node(eNode)
		self.add_edge(sNode, eNode, seq=seq)
		
	def get_seq(self, start=None, end=None):
		"""return the node's seq defined by start and end
		
		Parameters
		----------
		start: the start node, by default the first one
		end: the end node, by default the last one
		
		Returns
		-------
		seq: a string of the corresponding sequence
		
		
		"""
		seq = ''
		if start == None: start = self.tnodes[0]
		if end == None: end = self.tnodes[-1]
		try:path=list(nx.shortest_simple_paths(self, start, end))[0]
		except: raise varGraphError("Illegal path specification.\n" % (start, end))
		for s, e in zip(path[:-1], path[1:]):
			seq+=self[s][e]['seq']
		return seq
	
	def get_length(self):
		"""function that returns the length of the sequence of this SeqNode
		
		Returns
		-------
		int: the length of the sequence in int
		"""
		return len(self.get_seq())

	def get_rc(self):
		"""returns the reverse complementary sequence of the node's sequence
		
		Returns
		-------
		rc: the reverse complementary sequence in string type
		
		Raises
		------
		varGraphError
		
		"""
		seq=self.get_seq()
		try: rc=rc_seq(seq)
		except: raise varGraphError("Cannot get reverse complementary before seq specification.")
		return rc
	
	
	def add_breakpoint(self, p):
		"""add a breakpoint to the object, split it into more segments
		
		Parameters
		----------
		p: the position where the breakpoint is, 0-offset
		
		Returns
		-------
		0
		"""
		e1 = (p, '-')
		s1 = (p, '+')
		# find the two bounding nodes
		path=list(nx.shortest_simple_paths(self, self.tnodes[0], self.tnodes[-1]))[0]
		leftNode = None
		rightNode = None
		for s, e in zip(path[:-1], path[1:]):
			if s[0] < p and e[0] > p:
				leftNode=s
				rightNode=e
				break
		# if no need to add breakpoint, do nothing
		if leftNode == None: return 0
		# add nodes
		self.tnodes.append(e1)
		self.tnodes.append(s1)
		self.tnodes = sorted(self.tnodes, key=lambda a: a[0])
		self.add_node(e1)
		self.add_node(s1)
		seq = self.get_seq(start=leftNode, end=rightNode)
		leftPos = leftNode[0]
		rightPos = rightNode[0]
		ind = p-leftPos
		leftSeq = seq[:ind]
		rightSeq = seq[ind:]
		self.add_edge(leftNode, e1, seq=leftSeq)
		self.add_edge(s1, rightNode, seq=rightSeq)
		self.add_edge(e1, s1, seq='')
		self.remove_edge(leftNode, rightNode)
		return 0
		
	
	
		