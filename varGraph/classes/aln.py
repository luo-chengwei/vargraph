"""Base class for sequence alignment details.

The AlnDetails class allows storing alignment details between sequences; it also offers
basic operations on the alignments.
"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

class AlnDetails:
	"""The base class for alignment details"""
	def __init__(self):
		"""Initialize an object with start, end, strand, gaps, deletions details
		
		Parameters
		----------
		ids: the list of contigs in the alignment organized as:
				 [(sample1, contig1), (sample2, contig2)]
		coords: the list of (start, end, strand) of alignments, 0-offset
		gaps: a list of the 0-offset of gaps in alignment; defined as a tuple, with 1st
			element being the insertion position index, and the 2nd element being the 
			length of the consecutive gaps.
		deletions: a list of 0-offset of deletions in alignment; defined as a tupe, with
			1st element being the deletion position index, and the 2nd element being the
			length of the deletion.
		"""
		self.ids = []
		self.coords = []
		self.identity = 0

class Aln:
	"""The base class for operations with alignments"""
	
	def __init__(self):
		"""Initialize an object with original sequences, names, and the aligned sequences
		
		Parameters
		----------
		names: the IDs of the sequences
		seqs: the sequences of the input items that are aligned
		alns: the aligned detailed sequences
		"""
		self.names = []
		self.seqs = []
		self.alns = []
		self.identity_matrix = {}
		
	def select_centroid(self, seqs = None):
		"""Select the centroid given a group of sequences.
		The distance is defined as the 1-nucleotide identity
		
		Parameters
		----------
		seqs: the input sequences
		
		Returns
		-------
		centroid: the name of the centroid sequence
		"""
		if seqs is None: seqs = set(self.names)
		if len(seqs) <= 2: return seqs[0]
		D = {}
		for r, q in self.identity_matrix:
			if r not in seqs or q not in seqs: continue
			if r not in D: D[r] = []
			if q not in D: D[q] = []
			D[r].append(self.identity_matrix[(r, q)])
			D[q].append(self.identity_matrix[(r, q)])
		X = []
		for n in D: X.append([n, sum(D[n])/len(D[n])])
		centroid = sorted(X, key=lambda a: a[1], reverse = True)[0][0]
		return centroid
			
		
	def add_alignment(self, ref, query, ref_seq, query_seq, ref_aln, query_aln, identity):
		"""Add alignment into the object
		
		Parameters
		----------
		ref: the reference sequence ID
		query: the query sequence ID
		ref_seq: the sequence of the reference
		query_seq: the sequence of the query
		ref_aln: the alignment details object ( of the reference sequence
		query_aln: the alignment coordinates (start, end, strand) of the query sequence
		"""
		self.names.append([ref, query])
		self.seqs.append([ref_seq, query_seq])
		self.alns.append([ref_aln, query_aln])
		self.identity_matrix[(ref, query)] = identity
		