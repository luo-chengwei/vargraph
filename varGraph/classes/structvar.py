"""Base class for structure variation.

The StructVar class allows manipulations on structure variation.

"""

#    Copyright (C) 2016-2020 by
#    Chengwei Luo <luo.chengwei@gatech.edu>
#    All rights reserved.
#    MIT license.

import random, operator, string

class StructVar:
	"""Base class for structure variation.

    A StructVar stores sequences of the SV for each sample, the coordinates, the .
    
    Parameters
    ----------
    seqs: a dict that keyed by tuple with format (sample, contig) and valued by sequences
    coords: the corresponding sequence's coordinate
    samples1: the set of samples in group1
    samples2: the set of samples in group2
    
	"""
	
	def __init__(self):
		self.seqs = {}
		self.coords = {}
		self.samples1 = []
		self.samples2 = []
		
	def get_by_sample(self, sample):
		"""Given a sample name and returns the associated SV if any.
		
		Parameters
		----------
		sample: the sample ID in either samples1 or samples2
		
		Return
		------
		sv: list of sample-specific structure variations; the entries are lists with
			shape: [contig, seq, start_coord, end_coord]
		
		"""
		
		sv = []
		Ks = []
		for (s, contig) in self.seqs:
			if s!=sample: continue
			seq = self.seqs[(s, contig)]
			start, end = self.coords[(s, contig)]
			sv.append([contig, seq, star, end])
		return sv
	