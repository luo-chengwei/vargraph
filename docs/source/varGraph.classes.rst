varGraph.classes package
========================

Submodules
----------

varGraph.classes.aln module
---------------------------

.. automodule:: varGraph.classes.aln
    :members:
    :undoc-members:
    :show-inheritance:

varGraph.classes.metaseq module
-------------------------------

.. automodule:: varGraph.classes.metaseq
    :members:
    :undoc-members:
    :show-inheritance:

varGraph.classes.seqnode module
-------------------------------

.. automodule:: varGraph.classes.seqnode
    :members:
    :undoc-members:
    :show-inheritance:

varGraph.classes.structvar module
---------------------------------

.. automodule:: varGraph.classes.structvar
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: varGraph.classes
    :members:
    :undoc-members:
    :show-inheritance:
