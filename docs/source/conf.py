# conf.py

# Add autodoc and napoleon to the extensions list

extensions = ['sphinx.ext.autodoc', 'sphinxcontrib.napoleon']

import sphinx_rtd_theme

html_theme = "sphinx_rtd_theme"

html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]