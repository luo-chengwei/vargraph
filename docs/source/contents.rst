.. _contents:

varGraph documentation contents
===============================

.. toctree::
	:maxdepth: 2
	
	modules
	varGraph
	varGraph/classes
	varGraph/algorithms