varGraph package
================

Subpackages
-----------

.. toctree::

    varGraph.algorithms
    varGraph.classes
    varGraph.exception
    varGraph.utils

Submodules
----------

varGraph.release module
-----------------------

.. automodule:: varGraph.release
    :members:
    :undoc-members:
    :show-inheritance:

varGraph.version module
-----------------------

.. automodule:: varGraph.version
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: varGraph
    :members:
    :undoc-members:
    :show-inheritance:
