# varGraph #

The linear representation of assembled metagenomic sequences limits various downstream analyses 
due to the conflict between the simplicity of string linearity (e.g., fastA/Q format) 
and the complex genetic diversity (e.g., gene rearrangements and transfer events) 
often harbored within natural microbial populations. varGraph is to reconcile such conflict by leveraging
graph representation and API that could be embedded in the program to facilitate flexible zoom-in/out
in complexity regions and other operations.


### What is this repository for? ###

* varGraph for graph representation of series metagenomes

* Version: v0.1.0

* https://bitbucket.org/luo-chengwei/vargraph/

### How do I install varGraph? ###

* Installation:

	    $ hg clone https://bitbucket.org/luo-chengwei/vargraph
	
	    $ cd vargraph
	
	    $ python setup.py install
    
* Dependencies

	+ Python 2.7 or above
	
	+ NetworkX v1.10 or above
	
	+ BioPython v1.66 or above
	
* Deployment example

	   $ python
	
	   $ import vargraph as vg
	
	   $ meta_seqs = nucmer_init.init_from_nucmer(samples, fasta_dict, nucmer_dict)
	
	   $ meta_seq.to_GFA(outprefix)
	   
    + see more in tutorial examples

### API documentation ###

* see http://vargraph.readthedocs.org/

### Tutorials ###



### Citation ###

  Luo C., Rodriguez-R. M., Konstantinidis T.K., varGraph expands variation analysis in series metagenomes. (in prep).